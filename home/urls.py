from django.urls import path

from home.views import index, get_movies

urlpatterns = [
    path('', index, name='index'),
    path('get-movies/', get_movies, name='get-movies'),
]

import requests
from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import render


def index(request):
    return render(request, 'index.html')


def get_movies(request):
    movie_title = request.GET.get('movieTitle')
    web_service_url = settings.WEB_SERVICE_URL + "get-movies/"
    movies = requests.get(web_service_url, params={'movieTitle': movie_title}).json()
    return JsonResponse(data=movies)
